package sensingeggproject.omron.hvcc2wconfig.http.listener;

/**
 * ログアウトAPIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnLogoutListener {
	public void onSuccessful();
	public void onAccessTokenExpired();
	public void onTimeout();
	public void onFailed(int status);
}
