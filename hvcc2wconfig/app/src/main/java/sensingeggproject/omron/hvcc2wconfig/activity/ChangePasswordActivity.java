package sensingeggproject.omron.hvcc2wconfig.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.activity.LoginActivity;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleMessageDialog;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleProgressDialog;
import sensingeggproject.omron.hvcc2wconfig.http.WebAPIs;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnChangePasswordListener;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LoginUtil;

public class ChangePasswordActivity extends AppCompatActivity {

    private Context _context;
    private ImageButton _backButton;
    private TextView _titleText;
    private ImageButton _emptyButton;
    private ImageButton _acceptButton;
    private EditText _changePasswd;
    private EditText _changePasswdConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        _context = this;
        init();
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private void init() {
        _backButton = (ImageButton)findViewById(R.id.activity_header_button_left);
        _titleText = (TextView)findViewById(R.id.activity_header_text);
        _emptyButton = (ImageButton)findViewById(R.id.activity_header_button_right);
        _acceptButton = (ImageButton)findViewById(R.id.activity_change_password_accept_button);
        _changePasswd = (EditText)findViewById(R.id.activity_change_password_input);
        _changePasswdConfirm = (EditText)findViewById(R.id.activity_change_password_confirm_input);

        //
        _titleText.setText(R.string.change_password);
        _backButton.setImageResource(R.drawable.button_back);
        _backButton.setOnClickListener(new BackOnClickListener());
        _emptyButton.setEnabled(false);
        _emptyButton.setImageResource(R.color.transparent);

        //
        _acceptButton.setOnClickListener(new AcceptOnClickListener());
    }

    //----------------------------------------------
    // Listener Class
    //----------------------------------------------
    private class BackOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            finish();
        }
    }

    private class AcceptOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String passwd = _changePasswd.getText().toString();
            final String PasswdConfirm = _changePasswdConfirm.getText().toString();

            // zero check
            if (passwd.length() == 0 && PasswdConfirm.length() == 0 ||
                    passwd.length() <= 7 && PasswdConfirm.length() <= 7) {
                LogUtil.d("[changepassword]length:" + passwd.length() + "/" + PasswdConfirm.length());
                //警告ダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_WARNING,
                        getString(R.string.dialog_title_003),
                        getString(R.string.dialog_msg_009),
                        getString(R.string.dialog_posi_001),
                        false, null);
                msgDiag.show();

                return;
            }

            // match check
            if (!passwd.equals(PasswdConfirm)) {
                LogUtil.d("[changepassword]not equal:" + passwd + "/" + PasswdConfirm);
                //警告ダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_WARNING,
                        getString(R.string.dialog_title_003),
                        getString(R.string.dialog_msg_010),
                        getString(R.string.dialog_posi_001),
                        false, null);
                msgDiag.show();

                return;
            }

            //確認ダイアログ表示
            SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                @Override
                public void onPositiveClick() {
                    LogUtil.d("onPositiveClick");

                    WebAPIs api = new WebAPIs();

                    // プログレスダイアログ表示
                    final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context, getString(R.string.dialog_prg_001));
                    prgDiag.show();

                    api.changePassword(_context, PasswdConfirm, new OnChangePasswordListener() {
                        @Override
                        public void onSuccessful() {
                            LogUtil.d("[changepassword]success");
                            //プログレスダイアログ終了
                            prgDiag.dismiss();
                            //情報ダイアログ表示
                            final SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                                @Override
                                public void onPositiveClick() {
                                    LogUtil.d("onPositiveClick");
                                    finish();
                                }

                                @Override
                                public void onNegativeClick() {
                                    LogUtil.d("onNegativeClick");
                                }

                                @Override
                                public void onCancel() {
                                    LogUtil.d("onCancel");
                                }
                            };

                            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_INFORMATION,
                                    getString(R.string.dialog_title_002),
                                    getString(R.string.dialog_msg_011),
                                    getString(R.string.dialog_posi_001),
                                    false, listener);
                            msgDiag.show();
                        }

                        @Override
                        public void onInvalidParameter() {
                            LogUtil.d("[changepassword]invalidparameter");
                            //プログレスダイアログ終了
                            prgDiag.dismiss();
                            //エラーダイアログ表示
                            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                                    getString(R.string.dialog_title_001),
                                    getString(R.string.dialog_msg_012),
                                    getString(R.string.dialog_posi_001),
                                    false, null);
                            msgDiag.show();
                        }

                        @Override
                        public void onAccessTokenExpired() {
                            LogUtil.d("[changepassword]accessTokenExpired");
                            //ログイン情報破棄
                            LoginUtil.drop(_context);
                            //プログレスダイアログ終了
                            prgDiag.dismiss();
                            //エラーダイアログ表示
                            SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                                @Override
                                public void onPositiveClick() {
                                    LogUtil.d("onPositiveClick");
                                    //ログイン画面へ遷移
                                    Intent intent = new Intent(_context, LoginActivity.class);
                                    intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }

                                @Override
                                public void onNegativeClick() {
                                    LogUtil.d("onNegativeClick");
                                }

                                @Override
                                public void onCancel() {
                                    LogUtil.d("onCancel");
                                }
                            };

                            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                                    getString(R.string.dialog_title_001),
                                    getString(R.string.dialog_msg_008),
                                    getString(R.string.dialog_posi_001),
                                    false, listener);
                            msgDiag.show();
                        }

                        @Override
                        public void onTimeout() {
                            LogUtil.d("[changepassword]timeout");
                            //プログレスダイアログ終了
                            prgDiag.dismiss();
                            //エラーダイアログ表示
                            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                                    getString(R.string.dialog_title_001),
                                    getString(R.string.dialog_msg_003),
                                    getString(R.string.dialog_posi_001),
                                    false, null);
                            msgDiag.show();
                        }

                        @Override
                        public void onFailed(int status) {
                            LogUtil.d("[changepassword]failed");
                            //プログレスダイアログ終了
                            prgDiag.dismiss();
                            //エラーダイアログ表示
                            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                                    getString(R.string.dialog_title_001),
                                    getString(R.string.dialog_msg_006),
                                    getString(R.string.dialog_posi_001),
                                    false, null);
                            msgDiag.show();
                        }
                    });
                }

                @Override
                public void onNegativeClick() {
                    LogUtil.d("onNegativeClick");
                }

                @Override
                public void onCancel() {
                    LogUtil.d("onCancel");
                }
            };

            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_QUESTION,
                    getString(R.string.dialog_title_004),
                    getString(R.string.dialog_msg_014),
                    getString(R.string.dialog_posi_002),
                    getString(R.string.dialog_nega_002),
                    false, listener);
            msgDiag.show();
        }
    }

}
