package sensingeggproject.omron.hvcc2wconfig.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.EditText;
import android.content.DialogInterface.OnClickListener;
import android.widget.LinearLayout;

import java.util.Calendar;

import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleMessageDialog;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleProgressDialog;
import sensingeggproject.omron.hvcc2wconfig.http.WebAPIs;
import sensingeggproject.omron.hvcc2wconfig.defs.LoginType;

import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnForgetPasswordListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnGetApplicationListListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnLoginListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnPushNotificationListener;
import sensingeggproject.omron.hvcc2wconfig.model.AppData;
import sensingeggproject.omron.hvcc2wconfig.util.InputValidatorUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LoginUtil;
import sensingeggproject.omron.hvcc2wconfig.util.PreferenceUtil;

/**
 * Created by mmiyakaw on 2016/01/05.
 */
public class LoginActivity extends AppCompatActivity {

    private Context _context;
    private ImageButton _loginButton;
    private ImageButton _emailCancelButton;
    private ImageButton _passwordCancelButton;
    private Button _forgetButton;
    private EditText _editEmail;
    private EditText _editPassword;
    private LinearLayout _LinearPassword;
    private boolean _isForgetMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        _context = this;
        // 初期化
        init();
        // アクセストークンの有効期限を確認する
        if ( LoginUtil.checkAccessTokenExpiresIn(_context) ){
            //自動ログイン
            LogUtil.d("Auto Login");
            _editEmail.setText(PreferenceUtil.getAccountId(_context));
            Intent intent = new Intent(_context, MainMenuActivity.class);
            startActivity(intent);
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private void init() {
        _loginButton = (ImageButton)findViewById(R.id.activity_login_login_button);
        _loginButton.setOnClickListener(new LoginOnClickListener());
        _emailCancelButton = (ImageButton)findViewById(R.id.activity_login_email_cancel_button);
        _emailCancelButton.setOnClickListener(new EmailCancelOnClickListener());
        _passwordCancelButton = (ImageButton)findViewById((R.id.activity_login_password_cancel_button));
        _passwordCancelButton.setOnClickListener(new PasswordCalcelOnClickListener());
        _forgetButton = (Button)findViewById(R.id.activity_login_forget_password_button);
        _forgetButton.setOnClickListener(new ForgetPasswordOnClickListener());
        _editEmail = (EditText)findViewById(R.id.activity_login_email_input);
        _editPassword = (EditText)findViewById(R.id.activity_login_password_input);
        _LinearPassword = (LinearLayout)findViewById(R.id.activity_login_password_box_layout);
    }

    private void moveToMainMenuActivity() {
        WebAPIs api = new WebAPIs();

        final Intent intent = new Intent(this, MainMenuActivity.class);

        // validator
        if( false == InputValidatorUtil.isEmail( _editEmail.getText().toString().trim() ) ) {
            LogUtil.d(_editEmail.getText().toString().trim());
            //警告ダイアログ表示
            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_WARNING,
                    getString(R.string.dialog_title_003),
                    getString(R.string.dialog_msg_001),
                    getString(R.string.dialog_posi_001),
                    false,null);
            msgDiag.show();

            return;
        }

        // プログレスダイアログ表示
        final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
        prgDiag.show();

        // WebAPIs
        api.loginUserAccount(_context, _editEmail.getText().toString().trim(), _editPassword.getText().toString().trim(), new OnLoginListener() {
            @Override
            public void onSuccessful(String accessToken, int expiresIn) {
                LogUtil.d("[login]success");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                LoginUtil.save(_context, _editEmail.getText().toString().trim(), _editPassword.getText().toString().trim(), accessToken, expiresIn, LoginType.EMAIL);
                _editPassword.setText("");
                startActivity(intent);
            }

            @Override
            public void onTimeout() {
                LogUtil.e("[login]Timeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_003),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onFailed(final int status) {
                LogUtil.e("[login]failed");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_002),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }
        });

    }

    private void forgetPassword() {
        WebAPIs api = new WebAPIs();

        // validator
        if( false == InputValidatorUtil.isEmail( _editEmail.getText().toString().trim() ) ) {
            LogUtil.d(_editEmail.getText().toString().trim());
            //警告ダイアログ表示
            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_WARNING,
                    getString(R.string.dialog_title_003),
                    getString(R.string.dialog_msg_001),
                    getString(R.string.dialog_posi_001),
                    false,null);
            msgDiag.show();

            return;
        }

        // プログレスダイアログ表示
        final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
        prgDiag.show();

        api.forgetPassword(_context, _editEmail.getText().toString().trim(), new OnForgetPasswordListener() {
            @Override
            public void onSuccessful() {
                LogUtil.e("[forget]onSuccessful");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //通知ダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_INFORMATION,
                        getString(R.string.dialog_title_002),
                        getString(R.string.dialog_msg_004),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onCannotSendEmail() {
                LogUtil.e("[forget]onCannotSendEmail");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_005),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onAccountNotFound() {
                LogUtil.e("[forget]onAccountNotFound");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_007),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onTimeout() {
                LogUtil.e("[forget]onTimeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_003),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onFailed(final int status) {
                LogUtil.e("[forget]onFailed");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_006),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }
        });
    }

    //----------------------------------------------
    // Listener Class
    //----------------------------------------------
    private class LoginOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( _isForgetMode ) {
                forgetPassword();
            }
            else{
                moveToMainMenuActivity();
            }
        }
    }

    private class EmailCancelOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            _editEmail.setText("");
        }
    }

    private class PasswordCalcelOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            _editPassword.setText("");
        }
    }

    private class ForgetPasswordOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            _isForgetMode = !_isForgetMode;
            if ( _isForgetMode ) {
                _forgetButton.setText(R.string.back_to_login);
                _loginButton.setImageResource(R.drawable.button_password_reissue);
                _LinearPassword.setVisibility(View.INVISIBLE);
                _editPassword.setText("");
            } else {
                _forgetButton.setText(R.string.forget_password);
                _loginButton.setImageResource(R.drawable.button_login);
                _LinearPassword.setVisibility(View.VISIBLE);
                _editPassword.setText("");
            }
        }
    }

}
