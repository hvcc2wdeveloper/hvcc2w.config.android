package sensingeggproject.omron.hvcc2wconfig.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/**
 * ネットワーク情報の操作を行うユーティリティクラス。
 *
 * author: Covia Inc.
 */
public class NetworkUtil {

	/**
	 * ネットワークの接続状態を確認する。
	 * @param context
	 * @return
	 */
	public static boolean isOnline(Context context) {
		ConnectivityManager manager = (ConnectivityManager)
				context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = manager.getActiveNetworkInfo();

		if ( networkInfo != null ) {
			return networkInfo.isConnected();
		}

		return false;
	}

	/**
	 * Android端末のMACアドレスを取得する。
	 * @param context
	 * @return
	 */
	public static String getMacAddress(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		return wifiInfo.getMacAddress();
	}
}
