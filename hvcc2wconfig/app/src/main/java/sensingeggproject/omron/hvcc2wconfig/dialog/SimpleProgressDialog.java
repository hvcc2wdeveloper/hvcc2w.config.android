package sensingeggproject.omron.hvcc2wconfig.dialog;

import android.os.Bundle;
import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.util.FontUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;

import android.view.Window;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.LinearInterpolator;

public class SimpleProgressDialog extends Dialog {

    private static final int INTERVAL_MS = 1000;

    private RotateAnimation	_aminProgress = null;
    private LinearInterpolator _linearInterpolator = null;
    private String _text = "";
    private Context _context;
    private ImageView _imageView;
    private TextView _textView;

    public SimpleProgressDialog(Context context, String initText) {
        super(context);
        _context = context;
        _text = initText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_progress);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        // Set Font Asset
        FontUtil.overrideTypeFace(_context, findViewById(R.id.dialog_progress_layout));

        setup();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onWindowFocusChanged( boolean hasFocus ) {
        setProgressAnimation(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        setProgressAnimation(false);
    }

    private void setup() {
        _imageView = (ImageView)findViewById(R.id.dialog_progress_image);
        _textView = (TextView)findViewById(R.id.dialog_progress_text);
        _textView.setText(_text);
    }

    public void setText(String text) {
        _textView.setText(text);
    }

    private void setProgressAnimation(boolean isEnable) {

        if ( _aminProgress != null ) {
            _aminProgress.cancel();
            _aminProgress = null;
            _linearInterpolator = null;
        }

        if ( isEnable ) {
            _aminProgress = new RotateAnimation(0, 360,
                    ((float)_imageView.getWidth()) / 2, ((float)_imageView.getHeight() / 2));
            _linearInterpolator = new LinearInterpolator();
            _aminProgress.setDuration(INTERVAL_MS);
            _aminProgress.setRepeatCount(Animation.INFINITE);
            _aminProgress.setStartOffset(0);
            _aminProgress.setInterpolator(_linearInterpolator);
            _imageView.startAnimation(_aminProgress);
        }
    }
}
