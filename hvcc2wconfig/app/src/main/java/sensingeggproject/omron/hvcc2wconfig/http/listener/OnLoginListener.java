package sensingeggproject.omron.hvcc2wconfig.http.listener;

/**
 * ログインAPIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnLoginListener {
	public void onSuccessful(String accessToken, int expiresIn);
	public void onTimeout();
	public void onFailed(int status);
}
