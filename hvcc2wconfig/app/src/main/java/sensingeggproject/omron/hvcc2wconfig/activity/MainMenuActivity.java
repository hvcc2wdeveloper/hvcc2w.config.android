package sensingeggproject.omron.hvcc2wconfig.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.activity.LoginActivity;
import sensingeggproject.omron.hvcc2wconfig.defs.LoginType;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleMessageDialog;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleProgressDialog;
import sensingeggproject.omron.hvcc2wconfig.http.WebAPIs;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnLogoutListener;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LoginUtil;

public class MainMenuActivity extends AppCompatActivity {

    private Context _context;
    private ImageButton _emptyButton;
    private TextView _titleText;
    private ImageButton _settingButton;
    private ImageButton _pushButton;
    private ImageButton _logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        _context = this;
        init();
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private void init() {
        _emptyButton = (ImageButton)findViewById(R.id.activity_header_button_left);
        _titleText = (TextView)findViewById(R.id.activity_header_text);
        _settingButton = (ImageButton)findViewById(R.id.activity_header_button_right);
        _pushButton = (ImageButton)findViewById(R.id.activity_main_menu_push_button);
        _logoutButton = (ImageButton)findViewById(R.id.activity_main_menu_logout_button);

        //
        _titleText.setText(R.string.management_menu);
        _emptyButton.setEnabled(false);
        _emptyButton.setImageResource(R.color.transparent);
        _settingButton.setImageResource(R.drawable.button_setting);
        _settingButton.setOnClickListener(new SettingOnClickListener());

        //
        _pushButton.setOnClickListener(new PushNotificationOnClickListener());
        _logoutButton.setOnClickListener(new LogoutOnClickListener());
    }

    private void moveToChangePasswordActivity() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    private void moveToPushNotificationActivity() {
        Intent intent = new Intent(this, PushNotificationActivity.class);
        startActivity(intent);
    }

    //----------------------------------------------
    // Listener Class
    //----------------------------------------------
    private class SettingOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            moveToChangePasswordActivity();
        }
    }

    private class PushNotificationOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            moveToPushNotificationActivity();
        }
    }

    private class LogoutOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            WebAPIs api = new WebAPIs();

            // プログレスダイアログ表示
            final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
            prgDiag.show();

            api.logoutUserAccount(_context, new OnLogoutListener() {
                public void onSuccessful() {
                    LogUtil.d("[logout]success");
                    //プログレスダイアログ終了
                    prgDiag.dismiss();
                    LoginUtil.drop(_context);
                    finish();
                }

                @Override
                public void onAccessTokenExpired() {
                    LogUtil.e("[logout]AccessTokenExpired");
                    //ログイン情報破棄
                    LoginUtil.drop(_context);
                    //プログレスダイアログ終了
                    prgDiag.dismiss();
                    //エラーダイアログ表示
                    SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                        @Override
                        public void onPositiveClick() {
                            LogUtil.d("onPositiveClick");
                            //ログイン画面へ遷移
                            Intent intent = new Intent(_context, LoginActivity.class);
                            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }

                        @Override
                        public void onNegativeClick() {
                            LogUtil.d("onNegativeClick");
                        }

                        @Override
                        public void onCancel() {
                            LogUtil.d("onCancel");
                        }
                    };

                    SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                            getString(R.string.dialog_title_001),
                            getString(R.string.dialog_msg_008),
                            getString(R.string.dialog_posi_001),
                            false, listener);
                    msgDiag.show();
                }

                @Override
                public void onTimeout() {
                    LogUtil.e("[logout]Timeout");
                    //プログレスダイアログ終了
                    prgDiag.dismiss();
                    //エラーダイアログ表示
                    SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                            getString(R.string.dialog_title_001),
                            getString(R.string.dialog_msg_003),
                            getString(R.string.dialog_posi_001),
                            false, null);
                    msgDiag.show();
                }

                @Override
                public void onFailed(final int status) {
                    //SimpleMessageDialog(getString(R.string.LoginFailed));
                    LogUtil.e("[logout]failed");
                    //プログレスダイアログ終了
                    prgDiag.dismiss();
                    //エラーダイアログ表示
                    SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                            getString(R.string.dialog_title_001),
                            getString(R.string.dialog_msg_017),
                            getString(R.string.dialog_posi_001),
                            false, null);
                    msgDiag.show();
                }
            });
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //戻る
        if ( keyCode == KeyEvent.KEYCODE_BACK ) {
            //無効
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}
