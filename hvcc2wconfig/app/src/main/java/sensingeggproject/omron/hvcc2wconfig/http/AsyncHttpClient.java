package sensingeggproject.omron.hvcc2wconfig.http;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import sensingeggproject.omron.hvcc2wconfig.config.AppConfig;
import sensingeggproject.omron.hvcc2wconfig.defs.HttpStatus;
import sensingeggproject.omron.hvcc2wconfig.defs.WebAPIProperty;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;

/**
 * 非同期通信処理を行うクラス。
 *
 * author: GigasJapan Yuki Hashimoto
 */
public class AsyncHttpClient {

	// ―――――――――――――――――――――――――
	// 定数
	// ―――――――――――――――――――――――――
	/** タグ */
	private static final String TAG = AsyncHttpClient.class.getSimpleName();

	// ―――――――――――――――――――――――――
	// リスナー定義
	// ―――――――――――――――――――――――――
	public interface Listener {
		public void onReceived(String responceJSON);
		public void onTimeout();
		public void onFailed(int status);
	}

	// ―――――――――――――――――――――――――
	// クラス変数
	// ―――――――――――――――――――――――――
	/** 接続先 */
	private String _url;
	/** パラメタ */
	private String _params;
	/** ヘッダ情報 */
	private Map<String, String> _properties;
	/** リスナー */
	private Listener _listener;

	/** 通信タスク */
	private MessageTask _task;
	/** セキュアな通信タスク */
	private PostSecureMessageTask _secureTask;

	/** HTTPステータス */
	private int _status;

	// ―――――――――――――――――――――――――
	// コンストラクタ
	// ―――――――――――――――――――――――――
	public AsyncHttpClient() {
		_properties = new HashMap<>();
	}

	// ―――――――――――――――――――――――――
	// 実行メソッド
	// ―――――――――――――――――――――――――
	/**
	 * GET通信。
	 * @param url
	 * @param params
	 * @param listener
	 */
	public void get(String url, String params, Listener listener) {
		_url = url;
		_params = params;
		_listener = listener;

		if ( AppConfig.API_SECURE_ENABLE ) {
			_secureTask = new PostSecureMessageTask();
			_secureTask.execute();
		} else {
			_task = new MessageTask();
			_task.execute();
		}
	}

	/**
	 * 通信を中断する。
	 */
	public void cancel() {
		if ( AppConfig.API_SECURE_ENABLE ) {
			_secureTask.cancel(true);
		} else {
			_task.cancel(true);
		}
	}

	// ―――――――――――――――――――――――――
	// 設定メソッド
	// ―――――――――――――――――――――――――
	/**
	 * ヘッダー情報を追加する。
	 * @param key
	 * @param value
	 */
	public void addProperty(String key, String value) {
		if(key.equals(WebAPIProperty.VALUE_AUTHORIZATION)) {
			value = WebAPIProperty.VALUE_AUTHORIZATION + " " + value;
		}

		_properties.put(key, value);
	}

	// ―――――――――――――――――――――――――
	// 非同期タスク
	// ―――――――――――――――――――――――――
	/** セキュアな通信タスク */
	private class PostSecureMessageTask extends AsyncTask<String, Integer, String> {
		private HttpsURLConnection __conn;

		@Override
		protected String doInBackground(String... params) {
			String response = null;
			__conn = null;
			try {
				LogUtil.d("connecting...");
				// リクエスト
				URL url = null;
				if(_params != null) {
					url = new URL(_url + "?" + _params);
				} else {
					url = new URL(_url);
				}
				__conn = (HttpsURLConnection) url.openConnection();
				__conn.setRequestMethod("GET");
				__conn.setConnectTimeout(AppConfig.API_TIMEOUT_TIME);
				__conn.setReadTimeout(AppConfig.API_TIMEOUT_TIME);
				for (Map.Entry<String, String> entry : _properties.entrySet()) {
					__conn.setRequestProperty(entry.getKey(), entry.getValue());
				}
				__conn.connect();

				// レスポンス
				LogUtil.d("responce " + __conn.getResponseCode());
				if(__conn.getResponseCode() == HttpStatus.CODE_200) {
					StringBuilder sb = new StringBuilder();
					BufferedReader reader = new BufferedReader(new InputStreamReader(__conn.getInputStream()));
					String inputLine;
					while ((inputLine = reader.readLine()) != null) {
						sb.append(inputLine);
					}
					response = sb.toString();
				} else {
					_status = __conn.getResponseCode();
					return null;
				}
			} catch(MalformedURLException e) {
				// URL形式不正（こないはず）
				e.printStackTrace();
				_status = HttpStatus.CODE_404;
				return null;
			} catch(SocketTimeoutException e) {
				// タイムアウト
				e.printStackTrace();
				_status = -1;
				return null;
			} catch(UnknownHostException e) {
				// ホストが見つからない
				e.printStackTrace();
				_status = HttpStatus.CODE_500;
				return null;
			} catch(IOException e) {
				e.printStackTrace();
			}

			if( __conn != null ){
				LogUtil.d("disconnecting...");
				__conn.disconnect();
			}
			if( response == null ){
				LogUtil.e("http response is null.");
				return null;
			}
			return response.toString();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			LogUtil.d("task cancelled");

			if( __conn != null ){
				LogUtil.d("disconnecting...");
				__conn.disconnect();
			}
		}

		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);

			if(response != null) {
				_listener.onReceived(response);
			} else {
				if(_status == -1) {
					_listener.onTimeout();
				} else {
					_listener.onFailed(_status);
				}
			}
		}
	}

	/** 非セキュアな通信タスク */
	private class MessageTask extends AsyncTask<String, Integer, String> {
		private HttpURLConnection __conn;

		@Override
		protected String doInBackground(String... params) {
			String response = null;
			__conn = null;
			try {
				LogUtil.d("connecting...");
				// リクエスト
				URL url = null;
				if(_params != null) {
					url = new URL(_url + "?" + _params);
				} else {
					url = new URL(_url);
				}
				__conn = (HttpURLConnection) url.openConnection();
				__conn.setRequestMethod("GET");
				__conn.setConnectTimeout(AppConfig.API_TIMEOUT_TIME);
				__conn.setReadTimeout(AppConfig.API_TIMEOUT_TIME);
				for (Map.Entry<String, String> entry : _properties.entrySet()) {
					__conn.setRequestProperty(entry.getKey(), entry.getValue());
				}
				__conn.connect();

				// レスポンス
				LogUtil.d("responce " + __conn.getResponseCode());
				if(__conn.getResponseCode() == HttpStatus.CODE_200) {
					StringBuilder sb = new StringBuilder();
					BufferedReader reader = new BufferedReader(new InputStreamReader(__conn.getInputStream()));
					String inputLine;
					while ((inputLine = reader.readLine()) != null) {
						sb.append(inputLine);
					}
					response = sb.toString();
				} else {
					_status = __conn.getResponseCode();
					return null;
				}
			} catch(MalformedURLException e) {
				// URL形式不正（こないはず）
				e.printStackTrace();
				_status = HttpStatus.CODE_404;
				return null;
			} catch(SocketTimeoutException e) {
				// タイムアウト
				e.printStackTrace();
				_status = -1;
				return null;
			} catch(UnknownHostException e) {
				// ホストが見つからない
				e.printStackTrace();
				cancel(true);
				_status = HttpStatus.CODE_500;
			} catch(IOException e) {
				e.printStackTrace();
			}

			if( __conn != null ){
				LogUtil.d("disconnecting...");
				__conn.disconnect();
			}

			return response.toString();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			LogUtil.d("task cancelled");

			if( __conn != null ){
				LogUtil.d("disconnecting...");
				__conn.disconnect();
			}
		}

		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);

			if(response != null) {
				_listener.onReceived(response);
			} else {
				if(_status == -1) {
					_listener.onTimeout();
				} else {
					_listener.onFailed(_status);
				}
			}
		}
	}
}
