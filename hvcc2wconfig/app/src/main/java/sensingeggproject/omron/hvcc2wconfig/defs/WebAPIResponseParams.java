package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * WebAPIのレスポンスパラメタ名。
 *
 * author: Covia Inc.
 */
public enum WebAPIResponseParams {
	RESULT					("result"),
	CODE					("code"),
	MESSAGE					("msg"),
	ACCESS					("access"),
	TOKEN					("token"),
	EXPIRES_IN				("expiresIn"),
	APP_LIST				("appList"),
	CAMERA_LIST				("cameraList"),
	CAMERA_ID				("cameraId"),
	CAMERA_NAME				("cameraName"),
	CAMERA_MAC_ADDRESS		("cameraMacAddr"),
	OWNER_TYPE				("ownerType"),
	OWNER_EMAIL				("ownerEmail"),
	SHARED_USER_LIST		("sharedUserList"),
	SHARED_USER_EMAIL		("shareUserEmail"),
	NOTIFICATION			("notification"),
	NOTIFICATION_MOTION		("enableMotion"),
	NOTIFICATION_SOUND		("enableSound"),
	NOTIFICATION_TIMER		("enableTimer"),
	APP_ID					("appId"),
	APP_NAME				("appName"),
	APP_TYPE				("appType");

	private final String name;
	private WebAPIResponseParams(final String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
