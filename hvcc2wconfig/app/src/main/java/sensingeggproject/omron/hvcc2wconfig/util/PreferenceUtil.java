package sensingeggproject.omron.hvcc2wconfig.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Calendar;

import sensingeggproject.omron.hvcc2wconfig.defs.PreferenceKeys;
import sensingeggproject.omron.hvcc2wconfig.defs.LoginType;

/**
 * SharedPreferencesの操作を行うユーティリティクラス。
 *
 * author: GigasJapan Yuki Hashimoto
 */
public class PreferenceUtil {

	// ―――――――――――――――――――――――――
	// 当クラス内で使用するメソッド群
	// ―――――――――――――――――――――――――
	/**
	 * SharedPreferencesオブジェクトを取得する。
	 * @param context		コンテキスト
	 * @return				SharedPreferencesクラスのオブジェクト
	 */
	private static SharedPreferences getSharedPreferences(Context context) {
		SharedPreferences sp = context.getSharedPreferences(
				PreferenceKeys.PREFERENCE_NAME.name(),
				Context.MODE_PRIVATE);
		return sp;
	}

	/**
	 * Editorオブジェクトを取得する。
	 * @param context		コンテキスト
	 * @return				Editorクラスのオブジェクト
	 */
	private static Editor getEditor(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		Editor editor = sp.edit();
		return editor;
	}

	// ―――――――――――――――――――――――――
	// 外部クラスから使用するメソッド群
	// ―――――――――――――――――――――――――
	/**
	 * アカウントIDを設定する。
	 * @param context
	 * @param accountId
	 */
	public static void setAccountId(Context context, String accountId) {
		Editor editor = getEditor(context);
		editor.putString(PreferenceKeys.LOGIN_ACCOUNT_ID.name(), accountId);
		editor.commit();
	}

	/**
	 * アカウントIDを取得する。
	 * @param context
	 * @return
	 */
	public static String getAccountId(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getString(PreferenceKeys.LOGIN_ACCOUNT_ID.name(), null);
	}

	/**
	 * アカウントIDを破棄する。
	 * @param context
	 * @return
	 */
	public static void removeAccountId(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_ACCOUNT_ID.name());
		editor.commit();
	}

	/**
	 * パスワードを設定する。
	 * @param context
	 * @param password
	 */
	public static void setPassword(Context context, String password) {
		Editor editor = getEditor(context);
		editor.putString(PreferenceKeys.LOGIN_PASSWORD.name(), password);
		editor.commit();
	}

	/**
	 * パスワードを取得する。
	 * @param context
	 * @return
	 */
	public static String getPassword(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getString(PreferenceKeys.LOGIN_PASSWORD.name(), null);
	}

	/**
	 * パスワードを破棄する。
	 * @param context
	 * @return
	 */
	public static void removePassword(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_PASSWORD.name());
		editor.commit();
	}

	/**
	 * アクセストークンを設定する。
	 * @param context
	 * @param accessToken
	 */
	public static void setAccessToken(Context context, String accessToken) {
		Editor editor = getEditor(context);
		editor.putString(PreferenceKeys.LOGIN_ACCESS_TOKEN.name(), accessToken);
		editor.commit();
	}

	/**
	 * アクセストークンを取得する。
	 * @param context
	 * @return
	 */
	public static String getAccessToken(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getString(PreferenceKeys.LOGIN_ACCESS_TOKEN.name(), null);
	}

	/**
	 * アクセストークンを破棄する。
	 * @param context
	 * @return
	 */
	public static void removeAccessToken(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_ACCESS_TOKEN.name());
		editor.commit();
	}

	/**
	 * アクセストークンの有効期限を設定する。
	 * @param context
	 * @param expiresIn
	 */
	public static void setAccessTokenExpiresIn(Context context, int expiresIn) {
		Editor editor = getEditor(context);
		editor.putInt(PreferenceKeys.LOGIN_EXPIRES_IN.name(), expiresIn);
		editor.commit();
	}

	/**
	 * アクセストークンの有効期限を取得する。
	 * @param context
	 * @return
	 */
	public static int getAccessTokenExpiresIn(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getInt(PreferenceKeys.LOGIN_EXPIRES_IN.name(), -1);
	}

	/**
	 * アクセストークンの有効期限を破棄する。
	 * @param context
	 * @return
	 */
	public static void removeAccessTokenExpiresIn(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_EXPIRES_IN.name());
		editor.commit();
	}

	/**
	 * ログインした日時を設定する。
	 * @param context
	 * @param calendar
	 */
	public static void setLoginStartTime(Context context, Calendar calendar) {
		Editor editor = getEditor(context);
		editor.putLong(PreferenceKeys.LOGIN_START_TIME.name(), calendar.getTimeInMillis());
		editor.commit();
	}

	/**
	 * ログインした日時を取得する。
	 * @param context
	 * @return
	 */
	public static Calendar getLoginStartTime(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		long time = sp.getLong(PreferenceKeys.LOGIN_START_TIME.name(), -1);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);

		return calendar;
	}

	/**
	 * ログインした日時を破棄する。
	 * @param context
	 * @return
	 */
	public static void removeLoginStartTime(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_START_TIME.name());
		editor.commit();
	}

	/**
	 * ログイン状態の切れる日時を設定する。
	 * @param context
	 * @param calendar
	 */
	public static void setLoginEndTime(Context context, Calendar calendar) {
		Editor editor = getEditor(context);
		editor.putLong(PreferenceKeys.LOGIN_END_TIME.name(), calendar.getTimeInMillis());
		editor.commit();
	}

	/**
	 * ログイン状態の切れる日時を取得する。
	 * @param context
	 * @return
	 */
	public static Calendar getLoginEndTime(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		long time = sp.getLong(PreferenceKeys.LOGIN_END_TIME.name(), -1);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);

		return calendar;
	}

	/**
	 * ログイン状態の切れる日時を破棄する。
	 * @param context
	 * @return
	 */
	public static void removeLoginEndTime(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_END_TIME.name());
		editor.commit();
	}

	/**
	 * ログイン種別を設定する。
	 * @param context
	 * @param type
	 */
	public static void setLoginType(Context context, LoginType type) {
		Editor editor = getEditor(context);
		editor.putInt(PreferenceKeys.LOGIN_TYPE.name(), type.getId());
		editor.commit();
	}

	/**
	 * ログイン種別を取得する。
	 * @param context
	 * @return
	 */
	public static LoginType getLoginType(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		int id = sp.getInt(PreferenceKeys.LOGIN_TYPE.name(), LoginType.OTHER.getId());
		return LoginType.getType(id);
	}

	/**
	 * ログイン種別を破棄する。
	 * @param context
	 * @return
	 */
	public static void removeLoginType(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.LOGIN_TYPE.name());
		editor.commit();
	}


	/**
	 * GCMのレジストレーションIDを設定する。
	 * @param context
	 * @param registrationId
	 */
	public static void setGcmRegistrationId(Context context, String registrationId) {
		Editor editor = getEditor(context);
		editor.putString(PreferenceKeys.GCM_REGISTRATION_ID.name(), registrationId);
		editor.commit();
	}

	/**
	 * GCMのレジストレーションIDを取得する。
	 * IDが設定されていない場合は null を返却する。
	 * @param context
	 * @return
	 */
	public static String getGcmRegistrationId(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getString(PreferenceKeys.GCM_REGISTRATION_ID.name(), null);
	}

	/**
	 * GCMのレジストレーションIDを破棄する。
	 * @param context
	 * @return
	 */
	public static void removeGcmRegistrationId(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.GCM_REGISTRATION_ID.name());
		editor.commit();
	}

	/**
	 * レジストレーションIDを取得した時のアプリバージョン番号を設定する。
	 * @param context
	 * @param version
	 */
	public static void setGcmAppVersion(Context context, int version) {
		Editor editor = getEditor(context);
		editor.putInt(PreferenceKeys.GCM_APP_VERSION.name(), version);
		editor.commit();
	}

	/**
	 * レジストレーションIDを取得した時のアプリバージョン番号を取得する。
	 * @param context
	 * @return
	 */
	public static int getGcmAppVersion(Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return sp.getInt(PreferenceKeys.GCM_APP_VERSION.name(), Integer.MIN_VALUE);
	}

	/**
	 * レジストレーションIDを取得した時のアプリバージョン番号を破棄する。
	 * @param context
	 * @return
	 */
	public static void removeGcmAppVersion(Context context) {
		Editor editor = getEditor(context);
		editor.remove(PreferenceKeys.GCM_APP_VERSION.name());
		editor.commit();
	}
}
