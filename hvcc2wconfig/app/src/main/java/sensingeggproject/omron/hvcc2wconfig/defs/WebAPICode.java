package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * WebAPIの結果コード。
 *
 * author: Covia Inc.
 */
public enum WebAPICode {
	SUCCESS							("W2C00000"),		// WebAPIの処理が正常に行われた。
	INVALID_PARAMETER				("W2C00001"),		// WebAPIへのパラメータに誤りがあるため、WebAPIの処理が正常に完了しなかった。
	ALREADY_EXISTS					("W2C00002"),		// 既に存在するユーザアカウントを登録しようとした。
	NOT_FOUND						("W2C00003"),		// 情報が存在しない。
	INCLUDE_NON_OWNER_CAMERA		("W2C00011"),		// 所有者ではないカメラの設定を変更しようとした。
	INCLUDE_OWN_ADDRESS				("W2C00012"),		// 自身自身のアドレスを設定した。
	INCLUDE_NOT_SHARE_USER			("W2C00013"),		// 共有ユーザー以外のeメールを設定した。
	USER_NOT_FOUND					("W2C00014"),		// 対象ユーザが存在しない。
	CAMERA_NOT_FOUND				("W2C00015"),		// 対象カメラが存在しない。
	CANNOT_SEND_EMAIL				("W2C00101"),		// 指定したeメールへのメール送信に失敗した。
	HAS_NO_EMAIL					("W2C00201"),		// メールアドレスが存在しない
	ACCESS_TOKEN_EXPIRED			("W2C00401"),		// アクセストークンの有効期限が切れた。
	FAILURE							("W2C99999"),		// 想定外の異常が発生し、WebAPIの処理が正常に完了できなかった。

	TIMEOUT							("timeout");		// タイムアウト。

	private final String id;
	private WebAPICode(final String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public static WebAPICode getCode(String id) {
		for ( WebAPICode code : WebAPICode.values() ) {
			if ( code.getId().equals(id) ) {
				return code;
			}
		}
		return null;
	}
}
