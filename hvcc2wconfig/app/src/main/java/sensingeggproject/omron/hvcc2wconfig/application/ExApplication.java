package sensingeggproject.omron.hvcc2wconfig.application;

import android.app.Application;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.FontCache;

/**
 * 拡張アプリケーションクラス
 *
 * autuor: Covia Inc.
 */
public class ExApplication extends Application {

	// ―――――――――――――――――――――――――
	// 定数
	// ―――――――――――――――――――――――――
	/** タグ */
	private static final String TAG = ExApplication.class.getSimpleName();

	// ―――――――――――――――――――――――――
	// クラス変数
	// ―――――――――――――――――――――――――
	/** インスタンス */
	private static ExApplication _instance;

	/** フォント */
	private FontCache _fontCache;

	// ―――――――――――――――――――――――――
	// ライフサイクル
	// ―――――――――――――――――――――――――
	/**
	 * アプリケーションが開始した時の処理。
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		LogUtil.d();
		_instance = this;
		_fontCache = new FontCache();
	}

	/**
	 * アプリケーションが終了した時の処理。
	 */
	@Override
	public void onTerminate() {
		super.onTerminate();
		LogUtil.d();
	}

	// ―――――――――――――――――――――――――
	// インスタンス取得メソッド
	// ―――――――――――――――――――――――――
	public static ExApplication getInstance() {
		return _instance;
	}


	public FontCache getFontCache() {
		return _fontCache;
	}

}
