package sensingeggproject.omron.hvcc2wconfig.config;

/**
 * アプリケーションの各設定値を定義するクラス。
 *
 * author: Covia Inc.
 */
public class AppConfig {

	// ―――――――――――――――――――――――――
	// WebAPI
	// ―――――――――――――――――――――――――
	/** HTTP+SSL */
	public static final boolean API_SECURE_ENABLE = true;
	/** API Key */
	public static final String API_KEY = "XQci7iexp64ZCWRNQ83FYR3wWn8iCiyWwFHwfADz";
	/** Application ID */
	public static final int API_APPLICATION_ID = -1;
	/** OS Type */
	public static final int API_OS_TYPE = 1;
	/** Release Type */
	public static final int RELEASE_TYPE = 0;
	/** リクエストとレスポンスのタイムアウト時間（ミリ秒） */
	public static final int API_TIMEOUT_TIME = 5000;

	/** サーバープロトコル */
	public static final String API_PROTOCOL = "https";
	/** サーバードメイン （開発環境）*/
//	public static final String API_DOMAIN = "portaldev.hvc.omron.com";
	/** サーバードメイン （本番環境）*/
	public static final String API_DOMAIN = "portal.hvc.omron.com";
	/** ベースURL*/
	public static final String API_BASE_URL = API_PROTOCOL + "://" + API_DOMAIN;
	/** ベースPATH */
	public static final String API_BASE_PATH = "/hvcc2wadmin/api";
	/** ログインAPI */
	public static final String API_LOGIN = API_BASE_PATH + "/login.php";
	/** ログアウトAPI */
	public static final String API_LOGOUT = API_BASE_PATH + "/logout.php";
	/** アプリ一覧取得API */
	public static final String API_GET_APP_LIST = API_BASE_PATH + "/getAppList.php";
	/** パスワード変更API */
	public static final String API_CHANGE_PASSWORD = API_BASE_PATH + "/changePassword.php";
	/** パスワードリセットAPI */
	public static final String API_RESET_PASSWORD = API_BASE_PATH + "/forgetPassword.php";
	/** プッシュ通知送信API */
	public static final String API_SEND_PUSH = API_BASE_PATH + "/pushNotification.php";

	// ―――――――――――――――――――――――――
	// Wi-Fi
	// ―――――――――――――――――――――――――
	/** Wi-Fi状態変更のチェックインターバル */
	public static final int INTERVAL_WIFI_ENABLE_CHECK = 100;
	/** Wi-Fi状態変更のリミット */
	public static final int LIMIT_WIFI_ENABLE_CHECK = 10000;

	// ―――――――――――――――――――――――――
	// Font
	// ―――――――――――――――――――――――――
	public static final String REGULAR_FONT = "fonts/NotoSansCJKjp-Regular.ttf";
	public static final String BOLD_FONT = "fonts/NotoSansCJKjp-Bold.ttf";

}
