package sensingeggproject.omron.hvcc2wconfig.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Class for Display Utility
 *
 * Author: Covia Inc.
 */
public class DisplayUtil {

	/**
	 * Get the current orientation.
	 * @param context
	 * @return				PORTRAIT:Configuration.ORIENTATION_PORTRAIT LANDSCAPE:Configuration.ORIENTATION_LANDSCAPE
	 */
	public static int getOrientation(Context context) {
		Configuration config = context.getResources().getConfiguration();
		return config.orientation;
	}

	/**
	 * Set "PORTRAIT" to the orientation.
	 * @param activity
	 */
	public static void setOrientationPortrait(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	/**
	 * Set "LANDSCAPE" to the orientation.
	 * @param activity
	 */
	public static void setOrientationLandscape(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}

	/**
	 * Unlock the orientation lock of screen.
	 * @param activity
	 */
	public static void unlockOrientationLock(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}

	/**
	 * Get the display size
	 * @param activity
	 * @return
	 */
	public static Point getDisplaySize(Activity activity) {
		WindowManager wm = activity.getWindowManager();
		Display disp = wm.getDefaultDisplay();

		Point size = new Point();
		disp.getSize(size);

		return size;
	}

	/**
	 * Convert from DP to PX.
	 * @param context
	 * @param dp
	 * @return
	 */
	public static int convDpToPixel(Context context, float dp) {
		float destiny = context.getResources().getDisplayMetrics().density;
		return (int) (dp * destiny + 0.5f);
	}
}
