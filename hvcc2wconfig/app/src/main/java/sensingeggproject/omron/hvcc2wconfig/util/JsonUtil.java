package sensingeggproject.omron.hvcc2wconfig.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * JSONデータを操作するユーティリティクラス。
 *
 * author: Covia Inc.
 */
public class JsonUtil {

	/**
	 * String型のJSONデータをJSONObjectに変換する。
	 * @param jsonString
	 * @return
	 */
	public static JSONObject getJsonByString(final String jsonString) {
		if ( jsonString == null ) {
			return null;
		}

		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jsonObject;
	}

	/**
	 * JSONをString型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @return 文字列に変換した値
	 */
	public static String getStringByJson(final JSONObject jsonObject, final String fieldName) {
		return getStringByJson(jsonObject, fieldName, "");
	}

	/**
	 * JSONをString型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @param errorValue エラー時の値
	 * @return 文字列に変換した値
	 */
	public static String getStringByJson(final JSONObject jsonObject, final String fieldName, final String errorValue) {
		try {
			if ( jsonObject.isNull(fieldName) ) {
				return errorValue;
			} else {
				return jsonObject.getString(fieldName);
			}
		} catch (Exception e) {
			LogUtil.e(e.toString(), new Throwable());
			return errorValue;
		}
	}

	/**
	 * JSONをint型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @return 整数型に変換した値
	 */
	public static int getIntByJson(final JSONObject jsonObject, final String fieldName) {
		return getIntByJson(jsonObject, fieldName, -1);
	}

	/**
	 * JSONをint型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @param errorValue エラー時の値
	 * @return 整数型に変換した値
	 */
	public static int getIntByJson(final JSONObject jsonObject, final String fieldName, final int errorValue) {
		try {
			if ( jsonObject.isNull(fieldName) ||
						(jsonObject.getString(fieldName).isEmpty()) ) {
				return errorValue;
			} else {

				return Integer.valueOf(jsonObject.getString(fieldName)).intValue();

			}
		} catch (Exception e) {
			LogUtil.e(e.toString(), new Throwable());
			return errorValue;
		}
	}

	/**
	 * JSONをlong型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @return 長整数型に変換した値
	 */
	public static long getLongByJson(final JSONObject jsonObject, final String fieldName) {
		return getLongByJson(jsonObject, fieldName, -1);
	}

	/**
	 * JSONをlong型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @param errorValue エラー時の値
	 * @return 長整数型に変換した値
	 */
	public static long getLongByJson(final JSONObject jsonObject, final String fieldName,  final long errorValue) {
		try {
			if ( jsonObject.isNull(fieldName) ||
					(jsonObject.getString(fieldName).isEmpty()) ) {
				return errorValue;
			} else {
				return Long.valueOf(jsonObject.getString(fieldName))
						.longValue();
			}
		} catch (Exception e) {
			LogUtil.e(e.toString(), new Throwable());
			return errorValue;
		}
	}

	/**
	 * JSONをdouble型に変換する。
	 * @param jsonObject
	 * @param fieldName
	 * @return
	 */
	public static double getDoubleByJson(final JSONObject jsonObject, final String fieldName) {
		return getDoubleByJson(jsonObject, fieldName, -1);
	}

	/**
	 * JSONをdouble型に変換する。
	 * @param jsonObject
	 * @param fieldName
	 * @param errorValue
	 * @return
	 */
	public static double getDoubleByJson(final JSONObject jsonObject, final String fieldName, final double errorValue) {
		try {
			if ( jsonObject.isNull(fieldName) ||
					jsonObject.getString(fieldName).isEmpty() ) {
				return errorValue;
			} else {
				return Double.valueOf(jsonObject.getString(fieldName)).doubleValue();
			}
		} catch(Exception e) {
			LogUtil.e(e.toString(), new Throwable());
			return errorValue;
		}
	}

	/**
	 * JSONをboolean型に変換する。
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @return 真偽型に変換した値
	 */
	public static boolean getBooleanByJson(final JSONObject jsonObject, final String fieldName) {
		return getBooleanByJson(jsonObject, fieldName, false);
	}


	/**
	 * JSONをboolean型に変換する。
	 *
	 * @param jsonObject JSONオブジェクト
	 * @param fieldName フィールド名
	 * @param errorValue エラー時の値
	 * @return 真偽型に変換した値
	 */
	public static boolean getBooleanByJson(final JSONObject jsonObject, final String fieldName, final boolean errorValue) {
		try {
			if ( ( jsonObject.isNull(fieldName) ) ||
					( jsonObject.getString(fieldName).isEmpty() ) ) {
				return errorValue;
			} else {
				return Boolean.valueOf(jsonObject.getString(fieldName))
						.booleanValue();
			}
		} catch (Exception e) {
			LogUtil.e(e.toString(), new Throwable());
			return errorValue;
		}
	}

	/**
	 * JSONオブジェクトから指定のフィールドのJSONオブジェクトを取得する。
	 * @param jsonObject
	 * @param fieldName
	 * @return
	 */
	public static JSONObject getJsonObjectByJson(JSONObject jsonObject, String fieldName) {
		if ( jsonObject == null || fieldName == null ) {
			return null;
		}

		JSONObject result = null;

		try {
			result = jsonObject.getJSONObject(fieldName);
		} catch(JSONException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}

	/**
	 * JSONオブジェクトから指定のフィールドのJSON配列を取得する。
	 * @param jsonObject
	 * @param fieldName
	 * @return
	 */
	public static JSONObject[] getJsonArrayByJson(JSONObject jsonObject, String fieldName) {
		if ( jsonObject == null || fieldName == null ) {
			return null;
		}

		JSONArray array = null;
		ArrayList<JSONObject> list = new ArrayList<JSONObject>();

		try {
			array = jsonObject.getJSONArray(fieldName);
			for( int i=0; i<array.length(); i++ ) {
				JSONObject object = array.getJSONObject(i);
				list.add(object);
			}
		} catch(JSONException e) {
			try {
				JSONObject object = jsonObject.getJSONObject(fieldName);
				list.add(object);
			} catch(JSONException ee) {
				ee.printStackTrace();
				return null;
			}
		}

		return list.toArray(new JSONObject[0]);
	}

	/**
	 * JSON配列から指定の要素番号のJSONオブジェクトを取得する。
	 * @param jsonArray
	 * @param index
	 * @return
	 */
	public static JSONObject getJsonObjectByJsonArray(JSONArray jsonArray, int index) {
		if ( jsonArray == null ||
				jsonArray.length() == 0 || index < 0 ) {
			return null;
		}

		JSONObject result = null;

		try {
			result = jsonArray.getJSONObject(index);
		} catch(JSONException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}
}
