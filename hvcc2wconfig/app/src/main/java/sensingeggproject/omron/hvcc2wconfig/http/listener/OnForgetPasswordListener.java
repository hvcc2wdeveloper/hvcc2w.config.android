package sensingeggproject.omron.hvcc2wconfig.http.listener;

/**
 * パスワードリセットAPIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnForgetPasswordListener {
	public void onSuccessful();
	public void onCannotSendEmail();
	public void onAccountNotFound();
	public void onTimeout();
	public void onFailed(int status);
}
