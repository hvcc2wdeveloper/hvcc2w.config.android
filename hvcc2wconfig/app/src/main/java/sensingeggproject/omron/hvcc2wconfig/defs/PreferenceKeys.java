package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * SharedPreferencesのKey名を定義するクラス。
 *
 * author: Covia Inc.
 */
public enum PreferenceKeys {
    /** SharedPreferences名 */
    PREFERENCE_NAME,

    /** ログイン状態 */
    STATE_LOGIN,
    /** 接続カメラの設定状態 */
    STATE_CAMERA_SETTINGS,

	// ―――――――――――――――――――――――――
	// 接続カメラ情報
	// ―――――――――――――――――――――――――
	/** 接続カメラのID */
	CAMERA_ID,
	/** 接続カメラ名 */
	CAMERA_NAME,
	/** ユーザ種別 */
	CAMERA_USER_TYPE,

	/** カメラのスピーカーボリューム */
	CAMERA_SPEAKER_VOLUME,

	// ―――――――――――――――――――――――――
	// GoogleCloudMessaging情報
	// ―――――――――――――――――――――――――
	/** レジストレーションIDを取得した時のアプリバージョン番号 */
	GCM_APP_VERSION,
	/** レジストレーションID */
	GCM_REGISTRATION_ID,

	// ―――――――――――――――――――――――――
	// ログイン情報
	// ―――――――――――――――――――――――――
	/** アカウントID */
	LOGIN_ACCOUNT_ID,
	/** パスワード */
	LOGIN_PASSWORD,
	/** アクセストークン */
	LOGIN_ACCESS_TOKEN,
	/** アクセストークンの有効期限 */
	LOGIN_EXPIRES_IN,
	/** ログインした日時 */
	LOGIN_START_TIME,
	/** ログイン状態の切れる日時 */
	LOGIN_END_TIME,
	/** ログイン種別 */
	LOGIN_TYPE,
	/** 使用許諾表示フラグ */
	POLICY_ACCEPT_FLAG,

	// ―――――――――――――――――――――――――
	// 客層分析
	// ―――――――――――――――――――――――――
	/** 安定化パラメータｰリトライカウント */
	CA_SP_RETRY_COUNT,
	/** 安定化パラメータｰフレームカウント */
	CA_SP_FRAME_COUNT,
	/** 安定化パラメータｰ最小上下角度 */
	CA_SP_UD_MIN,
	/** 安定化パラメータｰ最大上下角度 */
	CA_SP_UD_MAX,
	/** 安定化パラメータｰ最小左右角度 */
	CA_SP_LR_MIN,
	/** 安定化パラメータｰ最大左右角度 */
	CA_SP_LR_MAX,
	/** 安定化パラメータｰ信頼度 */
	CA_SP_THRESHOLD_USE,

	// ―――――――――――――――――――――――――
	// 人数カウンター
	// ―――――――――――――――――――――――――
	/** 人数カウンターｰリトライカウント */
	PC_SP_RETRY_COUNT,

	// ―――――――――――――――――――――――――
	// 顔数カウンター
	// ―――――――――――――――――――――――――
	/** 顔数カウンターｰリトライカウント */
	FC_SP_RETRY_COUNT,

	// ―――――――――――――――――――――――――
	// 個人認証
	// ―――――――――――――――――――――――――
	/** 個人認証ｰリトライカウント */
	PA_SP_RETRY_COUNT,
	/** 個人認証ｰフレームカウント */
	PA_SP_FRAME_COUNT,
	/** 個人認証ｰ占拠率 */
	PA_SP_MIN_RATIO,
	/** 個人認証ｰ最小上下角度 */
	PA_SP_UD_MIN,
	/** 個人認証ｰ最大上下角度 */
	PA_SP_UD_MAX,
	/** 個人認証ｰ最小左右角度 */
	PA_SP_LR_MIN,
	/** 個人認証ｰ最大左右角度 */
	PA_SP_LR_MAX,
	/** 個人認証ｰ信頼度 */
	PA_SP_THRESHOLD_USE,

	// ―――――――――――――――――――――――――
	// 入退室管理
	// ―――――――――――――――――――――――――
	/** 入退室管理ｰリトライカウント */
	EE_SP_RETRY_COUNT,
	/** 入退室管理ｰフレームカウント */
	EE_SP_FRAME_COUNT,
	/** 入退室管理ｰ占拠率 */
	EE_SP_MIN_RATIO,
	/** 入退室管理ｰ最小上下角度 */
	EE_SP_UD_MIN,
	/** 入退室管理ｰ最大上下角度 */
	EE_SP_UD_MAX,
	/** 入退室管理ｰ最小左右角度 */
	EE_SP_LR_MIN,
	/** 入退室管理ｰ最大左右角度 */
	EE_SP_LR_MAX,
	/** 入退室管理ｰ信頼度 */
	EE_SP_THRESHOLD_USE,

}
