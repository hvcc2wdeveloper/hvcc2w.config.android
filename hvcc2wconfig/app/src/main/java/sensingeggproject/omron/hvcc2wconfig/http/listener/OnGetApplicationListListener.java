package sensingeggproject.omron.hvcc2wconfig.http.listener;

import sensingeggproject.omron.hvcc2wconfig.model.AppData;

/**
 * アプリケーションリスト取得APIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnGetApplicationListListener {
	public void onSuccessful(AppData[] appData);
	public void onAccessTokenExpired();
	public void onTimeout();
	public void onFailed(int status);
}
