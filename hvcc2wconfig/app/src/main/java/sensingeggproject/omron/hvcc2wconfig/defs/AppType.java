package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * アプリケーション区分。
 *
 * author: Covia Inc.
 */
public enum AppType {
	PUBLIC			(0),
	PRIVATE			(1),
	ADMINISTRATOR	(2),
	ALL				(9);


	private final int id;
	private AppType(final int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public static AppType getType(int id) {
		for( AppType type : AppType.values() ) {
			if ( type.getId() == id ) {
				return type;
			}
		}
		return null;
	}
}
