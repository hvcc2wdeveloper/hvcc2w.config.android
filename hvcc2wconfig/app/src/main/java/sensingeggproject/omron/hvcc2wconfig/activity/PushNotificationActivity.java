package sensingeggproject.omron.hvcc2wconfig.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleMessageDialog;
import sensingeggproject.omron.hvcc2wconfig.dialog.SimpleProgressDialog;
import sensingeggproject.omron.hvcc2wconfig.http.WebAPIs;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnGetApplicationListListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnPushNotificationListener;
import sensingeggproject.omron.hvcc2wconfig.model.AppData;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LoginUtil;

public class PushNotificationActivity extends AppCompatActivity {

    private static final int PUSH_TEXT_BYTES_MAX = 200;
    private Context _context;
    private Spinner _spinner;
    private ImageButton _backButton;
    private TextView _titleText;
    private ImageButton _emptyButton;
    private ImageButton _acceptButton;
    private EditText _delvMessage;
    private AppData[] _appDataList;
    // キーボード表示を制御するためのオブジェクト
    InputMethodManager _inputMethodManager;
    // 背景のレイアウト
    private LinearLayout _mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notification);
        _context = this;
        init();
    }

    // 画面タップ時の処理
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtil.d("TouchEvent " + "X:" + event.getX() + ",Y:" + event.getY());
        // 背景にフォーカスを移す
        _mainLayout.requestFocus();

        return true;

    }
    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private void init() {
        _backButton = (ImageButton)findViewById(R.id.activity_header_button_left);
        _titleText = (TextView)findViewById(R.id.activity_header_text);
        _emptyButton = (ImageButton)findViewById(R.id.activity_header_button_right);
        _acceptButton = (ImageButton)findViewById(R.id.activity_push_notification_accept_button);
        _spinner = (Spinner)findViewById(R.id.activity_push_notification_app_spinner);
        _delvMessage = (EditText)findViewById(R.id.activity_push_notification_text);
        _inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        _mainLayout = (LinearLayout) findViewById(R.id.main_layout);

        //
        _titleText.setText(R.string.send_push_notification);
        _backButton.setImageResource(R.drawable.button_back);
        _backButton.setOnClickListener(new BackOnClickListener());
        _emptyButton.setEnabled(false);
        _emptyButton.setImageResource(R.color.transparent);
        //
        _acceptButton.setOnClickListener(new AcceptOnClickListener());

        _delvMessage.setOnFocusChangeListener(new EditTextOnFocusChangeListener());
        _delvMessage.setOnKeyListener(new EditTextOnKeyListener());

        // AppList初期化
        initAppList();
    }

    private void initAppList(){
        final AppListAdapter<String> adapter = new AppListAdapter<>(_context, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getResources().getString(R.string.send_push_all_app));
        _spinner.setAdapter(adapter);
        _spinner.setSelection(0);
        _spinner.setFocusable(true);

        // プログレスダイアログ表示
        final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
        prgDiag.show();

        WebAPIs api = new WebAPIs();
        api.getApplicationList(_context, new OnGetApplicationListListener() {
            @Override
            public void onSuccessful(AppData[] appData) {
                _appDataList = appData;

                for (AppData app : _appDataList) {
                    adapter.add(app.getName());
                }

                //プログレスダイアログ終了
                prgDiag.dismiss();
            }

            @Override
            public void onAccessTokenExpired() {
                LogUtil.e("[getApplicationList]AccessTokenExpired");
                //ログイン情報破棄
                LoginUtil.drop(_context);
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //ダイアログリスナー
                SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                    @Override
                    public void onPositiveClick() {
                        LogUtil.d("onPositiveClick");
                        //ログイン画面へ遷移
                        Intent intent = new Intent(_context, LoginActivity.class);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    @Override
                    public void onNegativeClick() {
                        LogUtil.d("onNegativeClick");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d("onCancel");
                    }
                };
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_008),
                        getString(R.string.dialog_posi_001),
                        false, listener);
                msgDiag.show();
            }

            @Override
            public void onTimeout() {
                LogUtil.e("[getApplicationList]Timeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //ダイアログリスナー
                SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                    @Override
                    public void onPositiveClick() {
                        LogUtil.d("onPositiveClick");
                        finish();
                    }

                    @Override
                    public void onNegativeClick() {
                        LogUtil.d("onNegativeClick");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d("onCancel");
                    }
                };
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_003),
                        getString(R.string.dialog_posi_001),
                        false, listener);
                msgDiag.show();
            }

            @Override
            public void onFailed(int status) {
                LogUtil.e("[getApplicationList]Timeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //ダイアログリスナー
                SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                    @Override
                    public void onPositiveClick() {
                        LogUtil.d("onPositiveClick");
                        finish();
                    }

                    @Override
                    public void onNegativeClick() {
                        LogUtil.d("onNegativeClick");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d("onCancel");
                    }
                };
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_006),
                        getString(R.string.dialog_posi_001),
                        false, listener);
                msgDiag.show();
            }
        });
    }

    private void pushNotificationOneApp(String appId, String MsgTxt) {

        // プログレスダイアログ表示
        final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
        prgDiag.show();

        WebAPIs api = new WebAPIs();
        api.sendPushNotification(_context, appId, MsgTxt, new OnPushNotificationListener() {
            @Override
            public void onSuccessful() {
                LogUtil.d("[push]success");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //確認ダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_INFORMATION,
                        getString(R.string.dialog_title_002),
                        getString(R.string.dialog_msg_013),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onAccessTokenExpired() {
                LogUtil.d("[push]accessTokenExpired");
                //ログイン情報破棄
                LoginUtil.drop(_context);
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                    @Override
                    public void onPositiveClick() {
                        LogUtil.d("onPositiveClick");
                        //ログイン画面へ遷移
                        Intent intent = new Intent(_context, LoginActivity.class);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    @Override
                    public void onNegativeClick() {
                        LogUtil.d("onNegativeClick");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d("onCancel");
                    }
                };

                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_008),
                        getString(R.string.dialog_posi_001),
                        false, listener);
                msgDiag.show();
            }

            @Override
            public void onTimeout() {
                LogUtil.d("[push]timeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_003),
                        getString(R.string.dialog_posi_001),
                        false, null);
                msgDiag.show();
            }

            @Override
            public void onFailed(int status) {
                LogUtil.d("[push]failed");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_006),
                        getString(R.string.dialog_posi_001),
                        false, null);
                msgDiag.show();
            }
        });
    }

    private void pushNotificationAllApp(String MsgTxt) {

        // プログレスダイアログ表示
        final SimpleProgressDialog prgDiag = new SimpleProgressDialog(_context,getString(R.string.dialog_prg_001));
        prgDiag.show();

        //全Appに送信開始
        pushNotificationNextApp(0, prgDiag, MsgTxt);

    }

    private void pushNotificationNextApp(final int index, final SimpleProgressDialog prgDiag, final String MsgTxt){
        WebAPIs api = new WebAPIs();
        api.sendPushNotification(_context, _appDataList[index].getId(), MsgTxt, new OnPushNotificationListener() {
            @Override
            public void onSuccessful() {
                LogUtil.d("[push]success");
                if( index+1 < _appDataList.length) {
                    pushNotificationNextApp(index+1, prgDiag, MsgTxt);
                } else {
                    //プログレスダイアログ終了
                    prgDiag.dismiss();
                    //確認ダイアログ表示
                    SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_INFORMATION,
                            getString(R.string.dialog_title_002),
                            getString(R.string.dialog_msg_013),
                            getString(R.string.dialog_posi_001),
                            false,null);
                    msgDiag.show();
                }
            }

            @Override
            public void onAccessTokenExpired() {
                LogUtil.d("[push]accessTokenExpired");
                //ログイン情報破棄
                LoginUtil.drop(_context);
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                    @Override
                    public void onPositiveClick() {
                        LogUtil.d("onPositiveClick");
                        //ログイン画面へ遷移
                        Intent intent = new Intent(_context, LoginActivity.class);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    @Override
                    public void onNegativeClick() {
                        LogUtil.d("onNegativeClick");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d("onCancel");
                    }
                };

                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_008),
                        getString(R.string.dialog_posi_001),
                        false,listener);
                msgDiag.show();
            }

            @Override
            public void onTimeout() {
                LogUtil.d("[push]timeout");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_003),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }

            @Override
            public void onFailed(int status) {
                LogUtil.d("[push]failed");
                //プログレスダイアログ終了
                prgDiag.dismiss();
                //エラーダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_ERROR,
                        getString(R.string.dialog_title_001),
                        getString(R.string.dialog_msg_006),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
            }
        });
    }
    //----------------------------------------------
    // Listener Class
    //----------------------------------------------
    private class BackOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            finish();
        }
    }

    private class AcceptOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String MsgTxt = _delvMessage.getText().toString();

            //文字入力なし判定
            if(MsgTxt.length() <= 0) {
                //ワーニングダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_WARNING,
                        getString(R.string.dialog_title_003),
                        getString(R.string.dialog_msg_015),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
                return;
            }
            int byteLength = MsgTxt.getBytes().length;
            //文字数オーバー判定
            if(byteLength > PUSH_TEXT_BYTES_MAX) {
                LogUtil.w("Text Bytes Over " + MsgTxt+" -> "+byteLength);

                //ワーニングダイアログ表示
                SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context,SimpleMessageDialog.MD_ICON_WARNING,
                        getString(R.string.dialog_title_003),
                        getString(R.string.dialog_msg_016),
                        getString(R.string.dialog_posi_001),
                        false,null);
                msgDiag.show();
                return;
            }

            //確認ダイアログ表示
            SimpleMessageDialog.OnDismissListener listener = new SimpleMessageDialog.OnDismissListener() {
                @Override
                public void onPositiveClick() {
                    String MsgTxt = _delvMessage.getText().toString();

                    //AppList　選択位置取得
                    int position = _spinner.getSelectedItemPosition();

                    if( position == 0 ) {
                        //全てのアプリへ送信
                        pushNotificationAllApp(MsgTxt);
                    } else {
                        //指定アプリへ送信
                        AppData app = _appDataList[position-1];
                        pushNotificationOneApp(app.getId(), MsgTxt);
                    }
                }

                @Override
                public void onNegativeClick() {
                    LogUtil.d("onNegativeClick");
                }

                @Override
                public void onCancel() {
                    LogUtil.d("onCancel");
                }
            };

            SimpleMessageDialog msgDiag = new SimpleMessageDialog(_context, SimpleMessageDialog.MD_ICON_QUESTION,
                    getString(R.string.dialog_title_004),
                    getString(R.string.dialog_msg_018),
                    getString(R.string.dialog_posi_003),
                    getString(R.string.dialog_nega_002),
                    false, listener);
            msgDiag.show();
        }
    }
    private class EditTextOnFocusChangeListener implements View.OnFocusChangeListener{
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){
                //受け取った時
            }else{
                //離れた時
                // キーボードを隠す
                _inputMethodManager.hideSoftInputFromWindow(_mainLayout.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }
    private class EditTextOnKeyListener implements View.OnKeyListener{
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if((event.getAction() == KeyEvent.ACTION_DOWN) &&
              (keyCode == KeyEvent.KEYCODE_ENTER))
            {
                // 背景にフォーカスを移す
                _mainLayout.requestFocus();
                return true;
            }
            return false;
        }
    }

    //----------------------------------------------
    // private Class
    //----------------------------------------------
    public class AppListAdapter<T> extends ArrayAdapter<T> {

        private Context mContext;

        public AppListAdapter(Context context, int textViewResourceId) {
            super(context,textViewResourceId);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.
                        inflate(R.layout.spinner_selected_item, null);
            }
            String text = (String)getItem(position);
            TextView tv = (TextView)convertView.
                    findViewById(R.id.sample_selected_text_id);
            tv.setText(text);

            return convertView;
        }

        @Override
        public View getDropDownView(int position,
                                    View convertView, ViewGroup parent) {
            if(convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.
                        inflate(R.layout.spinner_item, null);
            }
            String text = (String)getItem(position);
            TextView tv = (TextView)convertView.
                    findViewById(R.id.sample_text_id);
            tv.setText(text);

            return convertView;
        }
    }
}
