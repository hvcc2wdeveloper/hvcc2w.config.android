package sensingeggproject.omron.hvcc2wconfig.util;

/**
 * Created by mmiyakaw on 2015/11/04.
 */
public class InputValidatorUtil {

    // ―――――――――――――――――――――――――
    // Constant Value
    // ―――――――――――――――――――――――――
    private static final String PATTERN_EMAIL = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static final String PATTERN_NUMERIC = "^\\-?[0-9]*\\.?[0-9]+$";

    // ―――――――――――――――――――――――――
    // External Method
    // ―――――――――――――――――――――――――
    public static boolean isEmail(String email) {
        return ( email.matches(PATTERN_EMAIL) ) ? true : false;
    }

    public static boolean isNumeric(String numeric) {
        return ( numeric.matches(PATTERN_NUMERIC) ) ? true : false;
    }

}
