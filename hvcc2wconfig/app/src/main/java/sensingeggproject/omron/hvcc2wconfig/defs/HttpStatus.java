package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * HTTPステータスコード。
 *
 * author: Covia Inc.
 */
public class HttpStatus {
	/** Web APIを正常に呼び出した */
	public static final int CODE_200 = 200;
	/** 不正なリクエスト（リクエストフォーマットが仕様と異なるなど）がされた */
	public static final int CODE_400 = 400;
	/** リクエストは認証を必要とする。認証が失敗した時など */
	public static final int CODE_401 = 401;
	/** リクエストしたWebAPIのURLが間違っている */
	public static final int CODE_404 = 404;
	/** リクエストがタイムアウトした */
	public static final int CODE_408 = 408;
	/** クラウドサーバー側の問題が原因でリクエストに失敗した */
	public static final int CODE_500 = 500;
}
