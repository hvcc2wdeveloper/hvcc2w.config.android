package sensingeggproject.omron.hvcc2wconfig.http.listener;

/**
 * パスワード変更APIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnChangePasswordListener {
	public void onSuccessful();
	public void onInvalidParameter();
	public void onAccessTokenExpired();
	public void onTimeout();
	public void onFailed(int status);
}
