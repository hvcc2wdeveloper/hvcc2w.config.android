package sensingeggproject.omron.hvcc2wconfig.util;

import android.content.Context;

import java.util.Calendar;

import sensingeggproject.omron.hvcc2wconfig.defs.LoginType;

/**
 * ログイン情報の操作を行うユーティリティクラス。
 *
 * author: Covia Inc.
 */
public class LoginUtil {

	/**
	 * ログイン情報を保持する。
	 * @param context
	 * @param email
	 * @param password
	 * @param accessToken
	 * @param expiresIn
	 */
	public static void save(Context context, String email, String password, String accessToken, int expiresIn, LoginType type) {
		Calendar loginStartCalendar = Calendar.getInstance();
		Calendar loginEndCalendar = Calendar.getInstance();
		loginEndCalendar.add(Calendar.HOUR_OF_DAY, expiresIn);

		PreferenceUtil.setAccountId(context, email);
		PreferenceUtil.setPassword(context, password);
		PreferenceUtil.setAccessToken(context, accessToken);
		PreferenceUtil.setAccessTokenExpiresIn(context, expiresIn);
		PreferenceUtil.setLoginStartTime(context, loginStartCalendar);
		PreferenceUtil.setLoginEndTime(context, loginEndCalendar);
		PreferenceUtil.setLoginType(context, type);

		LogUtil.d("\naccountId:" + email +
				"\naccessToken:" + accessToken +
				"\nexpiresIn:" + expiresIn +
				"\nloginStartTime:" + loginStartCalendar.getTime().toString() +
				"\nloginEndTime:" + loginEndCalendar.getTime().toString());
	}

	/**
	 * ログイン情報を破棄する。
	 * @param context
	 */
	public static void drop(Context context) {
		LogUtil.d();
		PreferenceUtil.removeAccountId(context);
		PreferenceUtil.removePassword(context);
		PreferenceUtil.removeAccessToken(context);
		PreferenceUtil.removeAccessTokenExpiresIn(context);
		PreferenceUtil.removeLoginStartTime(context);
		PreferenceUtil.removeLoginEndTime(context);
		PreferenceUtil.removeLoginType(context);
	}

	/**
	 * アクセストークンを取得する。
	 * @param context
	 * @return
	 */
	public static String getAccessToken(Context context) {
		return PreferenceUtil.getAccessToken(context);
	}

	/**
	 * アクセストークンの有効期限をチェックする
	 * @param context
	 * @return
	 */
	public static boolean checkAccessTokenExpiresIn(Context context) {
		String email = PreferenceUtil.getAccountId(context);
		String accessToken = PreferenceUtil.getAccessToken(context);
		int expiresIn= PreferenceUtil.getAccessTokenExpiresIn(context);
		Calendar startCalendar = PreferenceUtil.getLoginStartTime(context);
		Calendar endCalendar = PreferenceUtil.getLoginEndTime(context);

		LogUtil.d("\naccountId:" + email +
				"\naccessToken:" + accessToken +
				"\nexpiresIn:" + expiresIn +
				"\nloginStartTime:" + startCalendar.getTime().toString() +
				"\nloginEndTime:" + endCalendar.getTime().toString());

		// ログイン情報の欠損チェック
		if(email == null || accessToken == null || expiresIn == -1 ||
				startCalendar == null || endCalendar == null) {
			LogUtil.e("loss of login information.");
			return false;
		}

		if(expiresIn == 0) {
			// expiresInが無制限
			return true;
		}

		// 有効期限チェック
		Calendar currentCalendar = Calendar.getInstance();
		if ( currentCalendar.after(endCalendar) ) {
			// 期限切れ ログイン情報破棄
			LoginUtil.drop(context);
			return false;
		}

		return true;
	}
}
