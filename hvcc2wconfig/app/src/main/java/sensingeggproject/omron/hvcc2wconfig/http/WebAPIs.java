package sensingeggproject.omron.hvcc2wconfig.http;

import android.content.Context;

import org.apache.http.*;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import sensingeggproject.omron.hvcc2wconfig.config.AppConfig;
import sensingeggproject.omron.hvcc2wconfig.defs.HttpStatus;
import sensingeggproject.omron.hvcc2wconfig.defs.WebAPICode;
import sensingeggproject.omron.hvcc2wconfig.defs.WebAPIProperty;
import sensingeggproject.omron.hvcc2wconfig.defs.WebAPIRequestParams;
import sensingeggproject.omron.hvcc2wconfig.defs.WebAPIResponseParams;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnLoginListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnLogoutListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnChangePasswordListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnForgetPasswordListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnGetApplicationListListener;
import sensingeggproject.omron.hvcc2wconfig.http.listener.OnPushNotificationListener;
import sensingeggproject.omron.hvcc2wconfig.model.AppData;
import sensingeggproject.omron.hvcc2wconfig.defs.AppType;
import sensingeggproject.omron.hvcc2wconfig.util.JsonUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LoginUtil;
import sensingeggproject.omron.hvcc2wconfig.util.NetworkUtil;
import sensingeggproject.omron.hvcc2wconfig.util.PreferenceUtil;

/**
 * WebAPIにリクエストを送信するクラス。
 *
 * author: Covia Inc.
 */
public class WebAPIs {

	// ―――――――――――――――――――――――――
	// 定数
	// ―――――――――――――――――――――――――
	/** タグ */
	private static final String TAG = WebAPIs.class.getSimpleName();

	/** SNS TYPE */
	private static final String SNS_TYPE_FACEBOOK = "1";


	// ―――――――――――――――――――――――――
	// クラス変数
	// ―――――――――――――――――――――――――
	private AsyncHttpClient _client;

	// ―――――――――――――――――――――――――
	// キャンセルメソッド
	// ―――――――――――――――――――――――――
	/**
	 * 処理を中断する。
	 */
	public void cancel() {
		if(_client == null) {
			return;
		}

		_client.cancel();
	}

	// ―――――――――――――――――――――――――
	// 実行メソッド
	// ―――――――――――――――――――――――――
	/**
	 * ユーザアカウントにログインする。
	 * @param context
	 * @param email
	 * @param password
	 * @param listener
	 */
	public void loginUserAccount(Context context, String email, String password, final OnLoginListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_LOGIN;
		String macAddress = NetworkUtil.getMacAddress(context);
		Map<String, String> map = new HashMap<>();
		map.put(WebAPIRequestParams.API_KEY.toString(), AppConfig.API_KEY);
		map.put(WebAPIRequestParams.DEVICE_ID.toString(), macAddress);
		map.put(WebAPIRequestParams.EMAIL.toString(), email);
		map.put(WebAPIRequestParams.PASSWORD.toString(), password);
		String params = convPostParameter(map);

		LogUtil.d("[LOGIN] url:" + url);
		LogUtil.d("[LOGIN] params:" + params);

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_APPLICATION);
		_client.get(url, params, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzeLoginUserAccountJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	/**
	 * ユーザアカウントからログアウトする。
	 * @param context
	 * @param listener
	 */
	public void logoutUserAccount(Context context, final OnLogoutListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_LOGOUT;
		String accessToken = LoginUtil.getAccessToken(context);

		LogUtil.d("[LOGOUT] url:" + url);

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_CHARSET);
		_client.addProperty(WebAPIProperty.KEY_AUTHORIZATION, accessToken);
		_client.get(url, null, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzeLogoutUserAccountJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	/**
	 * アプリケー所のリストを取得する。
	 * @param context
	 * @param listener
	 */
	public void getApplicationList(Context context, final OnGetApplicationListListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_GET_APP_LIST;
		String accessToken = LoginUtil.getAccessToken(context);
		Map<String, String> map = new HashMap<>();
		map.put(WebAPIRequestParams.TYPE.toString(), "public");
		String params = convPostParameter(map);

		LogUtil.d("[GET_APP_LIST] url:" + url);
		LogUtil.d("[GET_APP_LIST] params:" + params);

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_CHARSET);
		_client.addProperty(WebAPIProperty.KEY_AUTHORIZATION, accessToken);
		_client.get(url, params, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzeGetApplicationListJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	/**
	 * パスワードを変更する。
	 * @param context
	 * @param password
	 * @param listener
	 */
	public void changePassword(Context context, String password, final OnChangePasswordListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_CHANGE_PASSWORD;
		String accessToken = LoginUtil.getAccessToken(context);

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_APPLICATION);
		_client.addProperty(WebAPIProperty.KEY_AUTHORIZATION, accessToken);
		Map<String, String> map = new HashMap<>();
		map.put(WebAPIRequestParams.NEW_PASSWORD.toString(), password);
		String params = convPostParameter(map);

		LogUtil.d("[CHANGE_PASSWORD] url:" + url);
		LogUtil.d("[CHANGE_PASSWORD] params:" + params);

		_client.get(url, params, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzeChangePasswordJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	/**
	 * パスワードをリセットする。
	 * @param context
	 * @param listener
	 */
	public void forgetPassword(Context context, String email, final OnForgetPasswordListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_RESET_PASSWORD;

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_APPLICATION);
		Map<String, String> map = new HashMap<>();
		map.put(WebAPIRequestParams.API_KEY.toString(), AppConfig.API_KEY);
		map.put(WebAPIRequestParams.EMAIL.toString(), email);
		String params = convPostParameter(map);

		LogUtil.d("[FORGET_PASSWORD] url:" + url);
		LogUtil.d("[FORGET_PASSWORD] params:" + params);

		_client.get(url, params, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzeForgetPasswordJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	/**
	 * パスワードを変更する。
	 * @param context
	 * @param appId
	 * @param text
	 * @param listener
	 */
	public void sendPushNotification(Context context, String appId, String text, final OnPushNotificationListener listener) {
		String url = AppConfig.API_BASE_URL + AppConfig.API_SEND_PUSH;
		String accessToken = LoginUtil.getAccessToken(context);

		_client = new AsyncHttpClient();
		_client.addProperty(WebAPIProperty.KEY_CONNECTION, WebAPIProperty.VALUE_CLOSE);
		_client.addProperty(WebAPIProperty.KEY_CONTENT_TYPE, WebAPIProperty.VALUE_APPLICATION);
		_client.addProperty(WebAPIProperty.KEY_AUTHORIZATION, accessToken);
		Map<String, String> map = new HashMap<>();
		if ( appId.isEmpty() || appId.length() == 0 ) {
			map.put(WebAPIRequestParams.APP_ID.toString(), "-1");
		} else {
			map.put(WebAPIRequestParams.APP_ID.toString(), appId);
		}
		map.put(WebAPIRequestParams.TEXT.toString(), getURLEncStr(text));
		String params = convPostParameter(map);

		LogUtil.d("[SEND_PUSH] url:" + url);
		LogUtil.d("[SEND_PUSH] params:" + params);

		_client.get(url, params, new AsyncHttpClient.Listener() {
			@Override
			public void onReceived(String responceJSON) {
				LogUtil.d(responceJSON);
				analyzePushNotificationJson(responceJSON, listener);
			}

			@Override
			public void onTimeout() {
				listener.onTimeout();
			}

			@Override
			public void onFailed(int status) {
				listener.onFailed(status);
			}
		});
	}

	// ―――――――――――――――――――――――――
	// JSONデータの解析メソッド
	// ―――――――――――――――――――――――――

	/**
	 * ログインのJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzeLoginUserAccountJson(String json, OnLoginListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if(code.equals(WebAPICode.SUCCESS.getId())) {
			JSONObject access = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.ACCESS.toString());
			String accessToken = JsonUtil.getStringByJson(access, WebAPIResponseParams.TOKEN.toString());
			int expiresIn = JsonUtil.getIntByJson(access, WebAPIResponseParams.EXPIRES_IN.toString());
			listener.onSuccessful(accessToken, expiresIn);
		} else {
			if ( code.equals(WebAPICode.HAS_NO_EMAIL.getId()) ) {
				listener.onFailed(HttpStatus.CODE_404);
			} else {
				listener.onFailed(HttpStatus.CODE_200);
			}
		}
	}

	/**
	 * ログアウトのJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzeLogoutUserAccountJson(String json, OnLogoutListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if(code.equals(WebAPICode.SUCCESS.getId())) {
			listener.onSuccessful();
		} else if(code.equals(WebAPICode.ACCESS_TOKEN_EXPIRED.getId())) {
			listener.onAccessTokenExpired();
		} else {
			listener.onFailed(HttpStatus.CODE_200);
		}
	}

	/**
	 * カメラリスト取得のJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzeGetApplicationListJson(String json, OnGetApplicationListListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if (code.equals(WebAPICode.SUCCESS.getId())) {
			JSONObject[] appList = JsonUtil.getJsonArrayByJson(
					root, WebAPIResponseParams.APP_LIST.toString());
			ArrayList<AppData> appDataList = new ArrayList<>();
			for (JSONObject app : appList) {
				AppData data = new AppData();
				data.setId(JsonUtil.getStringByJson(app, WebAPIResponseParams.APP_ID.toString()));
				data.setName(JsonUtil.getStringByJson(app, WebAPIResponseParams.APP_NAME.toString()));
				int appTypeI = JsonUtil.getIntByJson(app, WebAPIResponseParams.APP_TYPE.toString());
				data.setType(AppType.getType(appTypeI));

				appDataList.add(data);
			}

			listener.onSuccessful(appDataList.toArray(new AppData[0]));
		} else if (code.equals(WebAPICode.ACCESS_TOKEN_EXPIRED.getId())) {
			listener.onAccessTokenExpired();
		} else {
			listener.onFailed(HttpStatus.CODE_200);
		}
	}

	/**
	 * パスワード変更のJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzeChangePasswordJson(String json, OnChangePasswordListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if (code.equals(WebAPICode.SUCCESS.getId())) {
			listener.onSuccessful();
		} else if (code.equals(WebAPICode.ACCESS_TOKEN_EXPIRED.getId())) {
			listener.onAccessTokenExpired();
		} else if(code.equals(WebAPICode.INVALID_PARAMETER.getId())) {
			listener.onInvalidParameter();
		} else {
			listener.onFailed(HttpStatus.CODE_200);
		}
	}

	/**
	 * パスワードリセットのJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzeForgetPasswordJson(String json, OnForgetPasswordListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if ( code.equals(WebAPICode.SUCCESS.getId()) ) {
			listener.onSuccessful();
		} else if ( code.equals(WebAPICode.CANNOT_SEND_EMAIL.getId()) ) {
			listener.onCannotSendEmail();
		} else if ( code.equals(WebAPICode.NOT_FOUND.getId()) ) {
			listener.onAccountNotFound();
		} else {
			listener.onFailed(HttpStatus.CODE_200);
		}
	}

	/**
	 * プッシュ通知のJSONデータを解析する。
	 * @param json
	 * @param listener
	 */
	private void analyzePushNotificationJson(String json, OnPushNotificationListener listener) {
		JSONObject root = JsonUtil.getJsonByString(json);
		JSONObject result = JsonUtil.getJsonObjectByJson(root, WebAPIResponseParams.RESULT.toString());
		String code = JsonUtil.getStringByJson(result, WebAPIResponseParams.CODE.toString());
		if ( code.equals(WebAPICode.SUCCESS.getId()) ) {
			listener.onSuccessful();
		} else {
			listener.onAccessTokenExpired();
			listener.onAccessTokenExpired();
			listener.onTimeout();
			listener.onFailed(200);
		}
	}


	// ―――――――――――――――――――――――――
	// ユーティリティメソッド
	// ―――――――――――――――――――――――――
	/**
	 * Map型のパラメタをString型に変換する。
	 * @param map
	 * @return
	 */
	private String convPostParameter(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();
		String result;
		for (Map.Entry<String, String> entry : map.entrySet()) {
			sb.append(entry.getKey());
			sb.append("=");
			sb.append(entry.getValue());
			sb.append("&");
		}
		sb.deleteCharAt(sb.lastIndexOf("&"));
		return sb.toString();
	}

	/**
	 * Map型のパラメタをString型に変換する。
	 * @param map
	 * @return
	 */
	private String convPostParameters(Map<String, String[]> map) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String[]> entry : map.entrySet()) {
			String[] values = entry.getValue();
			if(values.length == 1) {
				sb.append(entry.getKey());
				sb.append("=");
				sb.append(values[0]);
				sb.append("&");
			} else {
				for(String value : values) {
					sb.append(entry.getKey());
					sb.append("[]=");
					sb.append(value);
					sb.append("&");
				}
			}
		}
		sb.deleteCharAt(sb.lastIndexOf("&"));

		return sb.toString();
	}

	public String getURLEncStr(String str){
		if(str == null) return null;
		try {
			return URLEncoder.encode(str , "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

}
