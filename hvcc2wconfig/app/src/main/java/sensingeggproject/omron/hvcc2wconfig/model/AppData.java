package sensingeggproject.omron.hvcc2wconfig.model;

import sensingeggproject.omron.hvcc2wconfig.defs.AppType;

import java.io.Serializable;

/**
 * アプリケーション情報のデータクラス。
 *
 * author: Covia Inc.
 */
public class AppData implements Serializable {

	private static final long serialVersionUID = -2422618031090499858L;

	/** APP_ID */
	private String id;
	/** アプリケーションの名前 */
	private String name;
	/** カメラの所有者区分 */
	private AppType type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AppType getType() {
		return type;
	}

	public void setType(AppType type) {
		this.type = type;
	}

}
