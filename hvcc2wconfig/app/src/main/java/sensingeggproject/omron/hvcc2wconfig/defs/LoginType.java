package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * ログイン種別。
 *
 * author: Covia Inc
 */
public enum LoginType {
	EMAIL		(0),
	FACEBOOK	(1),
	GOOGLE		(2),
	TWITTER     (3),
	OTHER		(99);

	private final int id;
	private LoginType(final int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public static LoginType getType(int id) {
		for( LoginType type : LoginType.values() ) {
			if( type.getId() == id ) {
				return type;
			}
		}

		return null;
	}
}
