package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * WebAPIのリクエストパラメタ名。
 *
 * author: Covia Inc.
 */
public enum WebAPIRequestParams {
	API_KEY				("apiKey"),
	APP_ID				("appId"),
	DEVICE_ID			("deviceId"),
	EMAIL				("email"),
	PASSWORD			("password"),
	OS_TYPE				("osType"),
	NOTIFICATION_ID		("notificationId"),
	SHARE_USER_EMAIL	("shareUserEmail"),
	CAMERA_ID			("cameraId"),
	IS_SHARED			("isShared"),
	NEW_PASSWORD		("newPassword"),
	NOTIFICATION_MOTION	("enableMotion"),
	NOTIFICATION_SOUND	("enableSound"),
	NOTIFICATION_TIMER	("enableTimer"),
	NEW_NAME			("newName"),
	LANGUAGE			("lang"),
	DEVICE_MODEL		("deviceModel"),
	DEVICE_OS_VERSION	("deviceOSVersion"),
	RELEASE_TYPE		("releaseType"),
	SNS_TYPE			("snsType"),
	SNS_ACCESS_TOKEN	("snsAccessToken"),
	TYPE				("type"),
	TEXT				("text");


	private final String name;
	private WebAPIRequestParams(final String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
