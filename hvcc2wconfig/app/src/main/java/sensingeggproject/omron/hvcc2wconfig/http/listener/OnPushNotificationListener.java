package sensingeggproject.omron.hvcc2wconfig.http.listener;

/**
 * プッシュ通知送信APIのリスナー。
 *
 * author: Covia Inc.
 */
public interface OnPushNotificationListener {
	public void onSuccessful();
	public void onAccessTokenExpired();
	public void onTimeout();
	public void onFailed(int status);
}
