package sensingeggproject.omron.hvcc2wconfig.defs;

/**
 * Created by kobayashi on 2015/07/02.
 */
public class WebAPIProperty {

	// ―――――――――――――――――――――――――
	// キー名
	// ―――――――――――――――――――――――――
	public static final String KEY_CONTENT_TYPE = "Content-Type";
	public static final String KEY_CONNECTION = "Connection";
	public static final String KEY_AUTHORIZATION = "Authorization";

	// ―――――――――――――――――――――――――
	// 値
	// ―――――――――――――――――――――――――
	public static final String VALUE_CHARSET = "charset=UTF-8";
	public static final String VALUE_APPLICATION = "application/x-www-form-urlencoded; charset=UTF-8";
	public static final String VALUE_CLOSE = "close";
	public static final String VALUE_AUTHORIZATION = "Bearer";
}
