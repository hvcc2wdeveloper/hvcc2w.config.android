package sensingeggproject.omron.hvcc2wconfig.activity;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.activity.LoginActivity;

public class SplashWindowActivity extends AppCompatActivity {

    private final Runnable _runDelayTask = new Runnable() {
        @Override
        public void run() {
            moveToLoginActivity();
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_window);
        new Handler().postDelayed(_runDelayTask, 3000);
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private void moveToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
