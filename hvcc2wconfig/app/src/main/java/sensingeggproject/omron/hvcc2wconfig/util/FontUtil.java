package sensingeggproject.omron.hvcc2wconfig.util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.graphics.Typeface;

import sensingeggproject.omron.hvcc2wconfig.application.ExApplication;

/**
 * Created by mmiyakaw on 2015/11/04.
 */
public class FontUtil {

    // ―――――――――――――――――――――――――
    // Constant Value
    // ―――――――――――――――――――――――――
    public static final String REGULAR_FONT = "fonts/NotoSansCJKjp-Regular.ttf";
    public static final String BOLD_FONT = "fonts/NotoSansCJKjp-Bold.ttf";
    // ―――――――――――――――――――――――――
    // External Method
    // ―――――――――――――――――――――――――
    public static void overrideTypeFace(Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideTypeFace(context, child);
                }
            } else if (v instanceof EditText ) {
                if ( ((EditText) v).getTypeface().getStyle() == Typeface.BOLD ||
                     ((EditText) v).getTypeface().getStyle() == Typeface.BOLD_ITALIC ) {
                    ((EditText) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(BOLD_FONT));
                } else {
                    ((EditText) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(REGULAR_FONT));
                }
            } else if (v instanceof TextView ) {
                if ( ((TextView) v).getTypeface().getStyle() == Typeface.BOLD ||
                        ((TextView) v).getTypeface().getStyle() == Typeface.BOLD_ITALIC ) {
                    ((TextView) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(BOLD_FONT));
                } else {
                    ((TextView) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(REGULAR_FONT));
                }
            } else if (v instanceof Button) {
                if ( ((Button) v).getTypeface().getStyle() == Typeface.BOLD ||
                        ((Button) v).getTypeface().getStyle() == Typeface.BOLD_ITALIC ) {
                    ((Button) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(BOLD_FONT));
                } else {
                    ((Button) v).setTypeface(ExApplication.getInstance().getFontCache().getFont(REGULAR_FONT));
                }
            }
        } catch (Exception e) {
        }
    }

}
