package sensingeggproject.omron.hvcc2wconfig.dialog;

import android.os.Bundle;
import sensingeggproject.omron.hvcc2wconfig.R;
import sensingeggproject.omron.hvcc2wconfig.util.FontUtil;
import sensingeggproject.omron.hvcc2wconfig.util.LogUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Button;
import android.view.WindowManager;

import android.content.DialogInterface;

public class SimpleMessageDialog extends Dialog {

    public static final int MD_ICON_ERROR          = 0x01;
    public static final int MD_ICON_WARNING        = 0x02;
    public static final int MD_ICON_INFORMATION    = 0x03;
    public static final int MD_ICON_QUESTION       = 0x04;

    public interface OnDismissListener {
        public void onPositiveClick();
        public void onNegativeClick();
        public void onCancel();
    };

    private Context _context;
    private boolean _isEnablePositive = false;
    private boolean _isEnableNegative = false;
    private String _title;
    private int _nIcon;
    private String _message;
    private String _positiveCaption;
    private String _negativeCaption;
    private boolean _isCancelable = false;
    private OnDismissListener _listener = null;
    private boolean _isListenrCalled = false;

    private ImageView _imageIcon;
    private TextView  _titleText;
    private TextView  _messageText;
    private Button _positiveButton;
    private Button _negativeButton;

    public SimpleMessageDialog(Context context, int nIcon, String title, String message,
                                        String buttonCaption, boolean isCancelable, OnDismissListener listener) {
        super(context);
        _context = context;
        _isEnablePositive = true;
        _isEnableNegative = false;
        _nIcon = nIcon;
        _title = title;
        _message = message;
        _positiveCaption = buttonCaption;
        _isCancelable = isCancelable;
        _listener = listener;
    }

    public SimpleMessageDialog(Context context, int nIcon, String title, String message,
         String positiveButtonCaption, String negativeButtonCaption, boolean isCancelable, OnDismissListener listener) {
        super(context);
        _context = context;
        _isEnablePositive = true;
        _isEnableNegative = true;
        _nIcon = nIcon;
        _title = title;
        _message = message;
        _positiveCaption = positiveButtonCaption;
        _negativeCaption = negativeButtonCaption;
        _isCancelable = isCancelable;
        _listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        // Set Font Asset
        FontUtil.overrideTypeFace(_context, findViewById(R.id.dialog_message_layout));

        setup();
    }

    @Override
    protected void onStop() {
        LogUtil.d("");
        if ( _listener != null ) {
            _listener.onCancel();
        }
        if ( !_isListenrCalled ) {
            dismiss();
        }
    }

    private void setup() {
        _imageIcon = (ImageView)findViewById(R.id.dialog_message_title_icon);
        _titleText = (TextView)findViewById(R.id.dialog_message_title_text);
        _messageText = (TextView)findViewById(R.id.dialog_message_text);
        _positiveButton = (Button)findViewById(R.id.dialog_message_button_positive);
        _negativeButton = (Button)findViewById(R.id.dialog_message_button_negative);

        _positiveButton.setVisibility((_isEnablePositive) ? View.VISIBLE : View.GONE);
        _negativeButton.setVisibility((_isEnableNegative) ? View.VISIBLE : View.GONE);

        //
        switch( _nIcon ) {
            case MD_ICON_ERROR:
                _imageIcon.setImageResource(R.drawable.m0_01_001);
                break;
            case MD_ICON_WARNING:
               _imageIcon.setImageResource(R.drawable.m0_01_002);
                break;
            case MD_ICON_QUESTION:
                _imageIcon.setImageResource(R.drawable.m0_01_004);
                break;
            case MD_ICON_INFORMATION:
            default:
                _imageIcon.setImageResource(R.drawable.m0_01_003);
                break;
        }
        _titleText.setText(_title);
        _messageText.setText(_message);
        _positiveButton.setText(_positiveCaption);
        _negativeButton.setText(_negativeCaption);
        _positiveButton.setOnClickListener(new PositiveButtonClickListener());
        _negativeButton.setOnClickListener(new NegativeButtonClickListener());
        _positiveButton.setClickable(true);
        _negativeButton.setClickable(true);

        setCancelable(_isCancelable);
    }

    // ―――――――――――――――――――――――――
    //  Event Listener
    // ―――――――――――――――――――――――――
    private class PositiveButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            LogUtil.d("");
            if ( _listener != null ) {
                _listener.onPositiveClick();
            }
            _isListenrCalled = true;
            dismiss();
        }
    }

    private class NegativeButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            LogUtil.d("");
            if ( _listener != null ) {
                _listener.onNegativeClick();
            }
            _isListenrCalled = true;
            dismiss();
        }
    }

}
