package sensingeggproject.omron.hvcc2wconfig.util;

import android.content.Context;
import android.graphics.Typeface;
import java.util.Map;
import java.util.HashMap;

import sensingeggproject.omron.hvcc2wconfig.application.ExApplication;

/**
 * Created by mmiyakaw on 2015/11/04.
 */
public class FontCache {

    private static Map<String, Typeface> fontMap = new HashMap<String, Typeface>();

    // ―――――――――――――――――――――――――
    // External Method
    // ―――――――――――――――――――――――――
    public static Typeface getFont(String fontName) {
        if ( fontMap.containsKey(fontName) ) {
            return fontMap.get(fontName);
        } else {
            Typeface tf = Typeface.createFromAsset(ExApplication.getInstance().getAssets(), fontName);
            fontMap.put(fontName, tf);
            return tf;
        }
    }

}
